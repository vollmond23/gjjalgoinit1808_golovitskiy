package com.getjavajob.training.algo.util;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;

import static java.util.Arrays.deepEquals;
import static java.util.Arrays.deepToString;

public class Assert {
    public static void assertEquals(String testName, int expected, int actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, BigInteger expected, BigInteger actual) {
        if (expected.equals(actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, double expected, double actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, boolean expected, boolean actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, String expected, String actual) {
        if (expected.equals(actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, double[] expected, double[] actual) {
        if (Arrays.equals(expected, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed.");
            System.out.println("Expected array:");
            for (double cell: expected) {
                System.out.printf("%f\t", cell);
            }
            System.out.println();
            System.out.println("Actual array:");
            for (double cell: actual) {
                System.out.printf("%f\t", cell);
            }
        }
    }

    public static void assertEquals(String testName, int[] expected, int[] actual) {
        if (Arrays.equals(expected, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed.");
            System.out.println("Expected array: " + Arrays.toString(expected));
            System.out.println("Actual array: " + Arrays.toString(actual));
        }
    }

    public static void assertEquals(String testName, int[][] expected, int[][] actual) {
        if (deepEquals(expected, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed.");
            System.out.println("Expected array: " + deepToString(expected));
            System.out.println("Actual array: " + deepToString(actual));
        }
    }

    public static void assertEquals(String testName, File expectedFile, File actualFile) throws IOException {
        byte[] bytesInExpectedFile = Files.readAllBytes(expectedFile.toPath());
        byte[] bytesInActualFile = Files.readAllBytes(actualFile.toPath());
        if (Arrays.equals(bytesInExpectedFile, bytesInActualFile)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed. Files are not equal.");
        }
    }

    public static void assertEquals(String testName, List<?> expected, List<?> actual) {
        if (expected.equals(actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed");
        }
    }
}