package com.getjavajob.training.algo.util;

import java.util.Scanner;

public class DataFromConsole {
    private static final Scanner inputLine = new Scanner(System.in);

    public static int getIntegerFromConsole() {
        int number;
        while (true) {
            if (inputLine.hasNextInt()) {
                number = inputLine.nextInt();
                break;
            } else {
                System.out.println("You should input an integer number.");
                inputLine.next();
            }
        }
        return number;
    }

    public static int getIntegerFromConsoleInRange(int min, int max) {
        int number;
        while(true) {
            number = getIntegerFromConsole();
            if (number < min || number > max) {
                System.out.println("Number should be between " + min + " and " + max + ".");
            } else {
                break;
            }
        }
        return number;
    }

    public static float getFloatFromConsole() {
        float number;
        while (true) {
            if (inputLine.hasNextFloat()) {
                number = inputLine.nextFloat();
                break;
            } else {
                System.out.println("You should input a float number.");
                inputLine.next();
            }
        }
        return number;
    }

    public static double getDoubleFromConsole() {
        double number;
        while (true) {
            if (inputLine.hasNextDouble()) {
                number = inputLine.nextDouble();
                break;
            } else {
                System.out.println("You should input a float number.");
                inputLine.next();
            }
        }
        return number;
    }

    public static String getStringFromConsole() {
        return inputLine.nextLine();
    }
}
