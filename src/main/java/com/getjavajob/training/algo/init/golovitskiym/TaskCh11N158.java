package com.getjavajob.training.algo.init.golovitskiym;

import java.util.Arrays;
import java.util.Random;

import static com.getjavajob.training.algo.util.DataFromConsole.getIntegerFromConsole;

public class TaskCh11N158 {
    public static void main(String[] args) {
        System.out.print("Input the size of array: ");
        int arraySize = getIntegerFromConsole();
        int[] array = new int[arraySize];
        Random randomNumber = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = randomNumber.nextInt(10);
        }
        System.out.print("You got an array: ");
        System.out.println(Arrays.toString(array));
        System.out.print("Result array: ");
        System.out.println(Arrays.toString(deleteRepeatedElements(array)));
    }

    static int[] deleteRepeatedElements(int[] array) {
        int[] resultArray = array.clone();
        int loopCount = resultArray.length;
        for (int i = 0; i < loopCount; i++) {
            for (int j = i + 1; j < loopCount; j++) {
                if (resultArray[j] == resultArray[i]) {
                    System.arraycopy(resultArray, 0, resultArray, 0, j);
                    System.arraycopy(resultArray, j + 1, resultArray, j, resultArray.length - j - 1);
                    resultArray[resultArray.length - 1] = 0;
                    loopCount--;
                }
            }
        }
        return resultArray;
    }
}
