package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.*;

public class TaskCh04N115 {
    public static void main(String[] args) {
        System.out.print("Input the year: ");
        int year = getIntegerFromConsole();
        System.out.println(getChineseYear(year));
    }

    static String getChineseYear(int year) {
        return getAnimal(year) + ", " + getColor(year);
    }

    private static String getColor(int year) {
        int difference = year - 1984;
        String color;
        switch (difference % 10) {
            case 0:
            case 1:
            case -9:
                color = "Green";
                break;
            case 2:
            case 3:
            case -8:
            case -7:
                color = "Red";
                break;
            case 4:
            case 5:
            case -6:
            case -5:
                color = "Yellow";
                break;
            case 6:
            case 7:
            case -4:
            case -3:
                color = "White";
                break;
            case 8:
            case 9:
            case -2:
            case -1:
                color = "Blue";
                break;
            default:
                color = "unknown";
                break;
        }
        return color;
    }

    private static String getAnimal(int year) {
        int difference = year - 1984;
        String animal;
        switch (difference % 12) {
            case 0:
                animal = "Rat";
                break;
            case 1:
            case -11:
                animal = "Ox";
                break;
            case 2:
            case -10:
                animal = "Tiger";
                break;
            case 3:
            case -9:
                animal = "Rabbit";
                break;
            case 4:
            case -8:
                animal = "Dragon";
                break;
            case 5:
            case -7:
                animal = "Snake";
                break;
            case 6:
            case -6:
                animal = "Horse";
                break;
            case 7:
            case -5:
                animal = "Sheep";
                break;
            case 8:
            case -4:
                animal = "Monkey";
                break;
            case 9:
            case -3:
                animal = "Rooster";
                break;
            case 10:
            case -2:
                animal = "Dog";
                break;
            case 11:
            case -1:
                animal = "Pig";
                break;
            default:
                animal = "unknown";
                break;
        }
        return animal;
    }
}
