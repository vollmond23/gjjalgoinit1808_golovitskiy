package com.getjavajob.training.algo.init.golovitskiym;

import java.util.*;

import static com.getjavajob.training.algo.util.DataFromConsole.getStringFromConsole;

public class TaskCh13N012 {
    public static void main(String[] args) {
        Database db = new Database();
        db.addEmployee("Ivanov", "Petr", "Semyonych", "Moscow", 2001, 1);
        db.addEmployee("Petrov", "Ivan", "Moscow", 2002, 2);
        db.addEmployee("Sidorov", "Mikhail", "Ivanovich", "Moscow", 2003, 3);
        db.addEmployee("Kozlov", "Andrey", "Artemovich", "Moscow", 2015, 4);
        db.addEmployee("Ivanova", "Ksenia", "Moscow", 2017, 5);
        db.addEmployee("Myshkin", "Knyaz", "Vladlenovich", "Moscow", 2001, 5);
        db.addEmployee("Novikov", "Sergey", "Sergeevich", "Moscow", 2015, 6);
        db.addEmployee("Belova", "Anastasia", "Petrovna", "Moscow", 2013, 7);
        db.addEmployee("Kruglov", "Evgeniy", "Moscow", 2015, 7);
        db.addEmployee("Dzyuba", "Artem", "Moscow", 2018, 1);
        db.addEmployee("Kashtanov", "Kashtan", "Kashtanovich", "Moscow", 2005, 4);
        db.addEmployee("Ivanov", "Leonid", "Semyonych", "Moscow", 2005, 2);
        db.addEmployee("Kramarev", "Saveliy", "Moscow", 2013, 12);
        db.addEmployee("Suchkov", "Ivan", "Vladimirovich", "Moscow", 2012, 11);
        db.addEmployee("Lipin", "Lavrentiy", "Pavlovich", "Moscow", 2015, 10);
        db.addEmployee("Kuklin", "Aleksey", "Evgenievich", "Moscow", 1998, 4);
        db.addEmployee("Kuklina", "Olga", "Vladimirovna", "Moscow", 1997, 6);
        db.addEmployee("Ryabov", "Mikhail", "Sergeevich", "Moscow", 2016, 5);
        db.addEmployee("Kirin", "Kirill", "Ashotovich", "Moscow", 2015, 7);
        db.addEmployee("Vasiliev", "Ashot", "Moscow", 2009, 1);
        System.out.print("Input string to find in database: ");
        db.printEmployees(db.findMatches(getStringFromConsole()));
    }
}

class Database {
    private List<Employee> employees = new ArrayList<>();

    public List<Employee> getEmployees() {
        return employees;
    }

    public void addEmployee(String surname, String name, String address, int hiringYear, int hiringMonth) {
        employees.add(new Employee(surname, name, address, new GregorianCalendar(hiringYear, hiringMonth, 1).getTime()));
    }

    public void addEmployee(String surname, String name, String patronymic, String address, int hiringYear, int hiringMonth) {
        employees.add(new Employee(surname, name, patronymic, address, new GregorianCalendar(hiringYear, hiringMonth, 1).getTime()));
    }

    public List<Employee> findMatches(String substringToFind) {
        List<Employee> matchesFounded = new ArrayList<>();
        for (Employee person : employees) {
            if (person.getSurname().toLowerCase().contains(substringToFind.toLowerCase())
                    || person.getName().toLowerCase().contains(substringToFind.toLowerCase())
                    || (person.getPatronymic() != null && person.getPatronymic().toLowerCase().contains(substringToFind.toLowerCase()))) {
                matchesFounded.add(person);
            }
        }
        return matchesFounded;
    }

    public void printEmployees(List<Employee> employees) {
        GregorianCalendar gc = new GregorianCalendar();
        int number = 1;
        if (employees.isEmpty()) {
            System.out.println("There are no employees in the list.");
        } else {
            System.out.println("There are " + employees.size() + " employees in the list: ");
            for (Employee employee : employees) {
                String patronymic = "";
                if (employee.getPatronymic() != null) {
                    patronymic = " " + employee.getPatronymic();
                }
                System.out.println(number++ + ":\t"
                        + employee.getSurname()
                        + " " + employee.getName()
                        + patronymic + ", works "
                        + employee.getWorkingPeriod(gc.get(Calendar.MONTH), gc.get(Calendar.YEAR)) + " years.");
            }
        }
    }
}

class Employee {
    private static final long SECONDS_IN_YEAR;

    static {
        SECONDS_IN_YEAR = 60 * 60 * 24 * 365;
    }

    private String surname;
    private String name;
    private String patronymic;
    private String address;
    private Date hiringDate;

    Employee(String surname, String name, String address, Date hiringDate) {
        this.surname = surname;
        this.name = name;
        this.address = address;
        this.hiringDate = hiringDate;
    }

    Employee(String surname, String name, String patronymic, String address, Date hiringDate) {
        this(surname, name, address, hiringDate);
        this.patronymic = patronymic;
    }

    String getSurname() {
        return surname;
    }

    void setSurname(String surname) {
        this.surname = surname;
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    String getPatronymic() {
        return patronymic;
    }

    void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    String getAddress() {
        return address;
    }

    void setAddress(String address) {
        this.address = address;
    }

    Date getHiringDate() {
        return hiringDate;
    }

    void setHiringDate(Date hiringDate) {
        this.hiringDate = hiringDate;
    }

    int getWorkingPeriod(int month, int year) {
        GregorianCalendar currentDate = new GregorianCalendar();
        currentDate.set(GregorianCalendar.YEAR, year);
        currentDate.set(GregorianCalendar.MONTH, month);
        long workingPeriod = (currentDate.getTime().getTime() - getHiringDate().getTime()) / 1000 / SECONDS_IN_YEAR;
        return (int) workingPeriod;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Employee)) return false;

        Employee employee = (Employee) o;

        if (!getSurname().equals(employee.getSurname())) return false;
        if (!getName().equals(employee.getName())) return false;
        if (getPatronymic() != null ? !getPatronymic().equals(employee.getPatronymic()) : employee.getPatronymic() != null)
            return false;
        if (!getAddress().equals(employee.getAddress())) return false;
        return getHiringDate().equals(employee.getHiringDate());
    }

    @Override
    public int hashCode() {
        int result = getSurname().hashCode();
        result = 31 * result + getName().hashCode();
        result = 31 * result + (getPatronymic() != null ? getPatronymic().hashCode() : 0);
        result = 31 * result + getAddress().hashCode();
        result = 31 * result + getHiringDate().hashCode();
        return result;
    }
}
