package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.golovitskiym.TaskCh02N013.*;

public class TaskCh02N013Test {
    public static void main(String[] args) {
        testReverseNumber();
    }

    private static void testReverseNumber() {
        assertEquals("TaskCh02N013Test.testReverseNumber", 321, reverseNumber(123));
    }
}
