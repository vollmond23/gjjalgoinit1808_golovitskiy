package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.*;

public class TaskCh09N015 {
    public static void main(String[] args) {
        System.out.print("Input a word: ");
        String word = getStringFromConsole();
        System.out.print("Input an index: ");
        char symbol = getChar(word, getIntegerFromConsole());
        System.out.println("The symbol is \'" + symbol + "\'");
    }

    static char getChar(String word, int index) {
        int innerIndex = index;
        while (true) {
            if (innerIndex < 0 || innerIndex >= word.length()) {
                System.out.print("Index is out of word length. Input another one: ");
                innerIndex = getIntegerFromConsole();
            } else {
                return word.charAt(innerIndex);
            }
        }
    }
}
