package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh12N028.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N028Test {
    public static void main(String[] args) {
        testFillUpArray();
    }

    private static void testFillUpArray() {
        int[][] expectedArray = {
                {1, 2, 3, 4, 5},
                {16, 17, 18, 19, 6},
                {15, 24, 25, 20, 7},
                {14, 23, 22, 21, 8},
                {13, 12, 11, 10, 9}};
        assertEquals("TaskCh12N028Test.testFillUpArray", expectedArray, fillUpArray(5));
    }
}
