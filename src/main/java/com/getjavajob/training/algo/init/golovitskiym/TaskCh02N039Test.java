package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh02N039.*;
import static com.getjavajob.training.algo.util.Assert.*;

public class TaskCh02N039Test {
    public static void main(String[] args) {
        testGetAngle();
    }

    private static void testGetAngle() {
        assertEquals("TaskCh02N039Test.testGetAngle", 105.0, getAngle(3, 30, 0));
    }
}
