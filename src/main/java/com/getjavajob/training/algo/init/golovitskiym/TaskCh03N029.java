package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.*;

public class TaskCh03N029 {
    public static void main(String[] args) {
        System.out.println("Input X: ");
        int inputX = getIntegerFromConsole();
        System.out.println("Input Y: ");
        int inputY = getIntegerFromConsole();
        System.out.println("Input Z: ");
        int inputZ = getIntegerFromConsole();
        System.out.println("Solution for subtask A: " + getResultA(inputX, inputY));
        System.out.println("Solution for subtask B: " + getResultB(inputX, inputY));
        System.out.println("Solution for subtask V: " + getResultV(inputX, inputY));
        System.out.println("Solution for subtask G: " + getResultG(inputX, inputY, inputZ));
        System.out.println("Solution for subtask D: " + getResultD(inputX, inputY, inputZ));
        System.out.println("Solution for subtask E: " + getResultE(inputX, inputY, inputZ));
    }

    /*
     * Method returns true if each of the numbers X and Y is even
     * @param x, y - the integer numbers input by user
     * */
    static boolean getResultA(int x, int y) {
        return x % 2 != 0 && y % 2 != 0;
    }

    /*
     * Method returns true if only one of the numbers X and Y is less then 20
     * @param x, y - the integer numbers input by user
     * */
    static boolean getResultB(int x, int y) {
        boolean expressionFirst = x < 20 && y > 20;
        boolean expressionSecond = x > 20 && y < 20;
        return expressionFirst || expressionSecond;
    }

    /*
     * Method returns true if at least one of the numbers X and Y is equal 0
     * @param x, y - the integer numbers input by user
     * */
    static boolean getResultV(int x, int y) {
        return x == 0 || y == 0;
    }

    /*
     * Method returns true if all of the numbers X, Y and Z are less then 0
     * @param x, y, z - the integer numbers input by user
     * */
    static boolean getResultG(int x, int y, int z) {
        return x < 0 && y < 0 && z < 0;
    }

    /*
     * Method returns true if only one of the numbers X, Y and Z is a multiple of five
     * @param x, y - the integer numbers input by user
     * */
    static boolean getResultD(int x, int y, int z) {
        boolean expressionFirst = x % 5 == 0 && y % 5 != 0 && z % 5 != 0;
        boolean expressionSecond = x % 5 != 0 && y % 5 == 0 && z % 5 != 0;
        boolean expressionThird = x % 5 != 0 && y % 5 != 0 && z % 5 == 0;

        return expressionFirst || expressionSecond || expressionThird;
    }

    /*
     * Method returns true if at least one of the numbers X, Y and Z is more then 100
     * @param x, y, z - the integer numbers input by user
     * */
    static boolean getResultE(int x, int y, int z) {
        return x > 100 || y > 100 || z > 100;
    }
}