package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.*;

public class TaskCh06N087 {
    public static void main(String[] args) {
        Game game = new Game();
        game.play();
    }
}

class Game {
    private String team1Name;
    private String team2Name;
    private int team1Score;
    private int team2Score;

    public String getTeam1Name() {
        return team1Name;
    }

    public void setTeam1Name(String team1Name) {
        this.team1Name = team1Name;
    }

    public String getTeam2Name() {
        return team2Name;
    }

    public void setTeam2Name(String team2Name) {
        this.team2Name = team2Name;
    }

    public int getTeam1Score() {
        return team1Score;
    }

    public void setTeam1Score(int team1Score) {
        this.team1Score = team1Score;
    }

    public int getTeam2Score() {
        return team2Score;
    }

    public void setTeam2Score(int team2Score) {
        this.team2Score = team2Score;
    }

    void play() {
        System.out.print("Enter team #1: ");
        this.team1Name = getStringFromConsole();
        System.out.print("Enter team #2: ");
        this.team2Name = getStringFromConsole();
        boolean stillPlaying = true;
        do {
            System.out.println(score());
            System.out.print("Enter team to score (1 or 2 or 0 to finish game): ");
            switch (getIntegerFromConsole()) {
                case 0:
                    stillPlaying = false;
                    System.out.println(score());
                    System.out.println(result());
                    break;
                case 1:
                    calculateScore(1, getAndCheckScore());
                    break;
                case 2:
                    calculateScore(2, getAndCheckScore());
                    break;
                default:
                    System.out.println("Wrong entered number!");
            }
        } while (stillPlaying);
    }

    String score() {
        return "Score: " + team1Name + " " + team1Score + " - " + team2Name + " " + team2Score;
    }

    String result() {
        String result;
        if (team1Score > team2Score) {
            result = "The winner is " + team1Name;
        } else if (team1Score < team2Score) {
            result = "The winner is " + team2Name;
        } else {
            result = "Draw!";
        }
        return result;
    }

    private void calculateScore(int teamNumber, int score) {
        boolean isCalculate = false;
        while (!isCalculate) {
            switch (teamNumber) {
                case 1:
                    team1Score += score;
                    isCalculate = true;
                    break;
                case 2:
                    team2Score += score;
                    isCalculate = true;
                    break;
                default:
                    System.out.println("We have 2 teams. Enter 1 or 2: ");
            }
        }
    }

    private int getAndCheckScore() {
        int inputScore;
        System.out.print("Enter score (1 or 2 or 3): ");
        while (true) {
            inputScore = getIntegerFromConsole();
            if (inputScore < 1 || inputScore > 3) {
                System.out.print("Score must be equal 1 or 2 or 3. Enter correct number: ");
            } else {
                break;
            }
        }
        return inputScore;
    }
}
