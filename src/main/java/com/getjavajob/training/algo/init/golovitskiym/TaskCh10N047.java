package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.getIntegerFromConsole;

public class TaskCh10N047 {
    public static void main(String[] args) {
        System.out.print("Input the index of random member of Fibonacci sequence: ");
        int index = getIntegerFromConsole();
        System.out.println("The member is " + findMember(index));
    }

    static long findMember(int index) {
        if (index == 0) {
            return 0;
        }
        if (index == 1 || index == 2) {
            return 1;
        }
        return findMember(index - 2) + findMember(index - 1);
    }
}
