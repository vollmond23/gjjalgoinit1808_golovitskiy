package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh12N234.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N234Test {
    public static void main(String[] args) {
        testDeleteRow();
        testDeleteColumn();
    }

    private static void testDeleteRow() {
        int[][] initialArray = {
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12},
                {13, 14, 15, 16}};
        int[][] expectedArray = {
                {1, 2, 3, 4},
                {9, 10, 11, 12},
                {13, 14, 15, 16},
                {0, 0, 0, 0}};
        assertEquals("TaskCh12N234Test.testDeleteRow", expectedArray, deleteRow(initialArray, 2));
    }

    private static void testDeleteColumn() {
        int[][] initialArray = {
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12},
                {13, 14, 15, 16}};
        int[][] expectedArray = {
                {1, 3, 4, 0},
                {5, 7, 8, 0},
                {9, 11, 12, 0},
                {13, 15, 16, 0}};
        assertEquals("TaskCh12N234Test.testDeleteColumn", expectedArray, deleteColumn(initialArray, 2));
    }
}
