package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh09N015.*;
import static com.getjavajob.training.algo.util.Assert.*;

public class TaskCh09N015Test {
    public static void main(String[] args) {
        testGetChar();
    }

    private static void testGetChar() {
        assertEquals("TaskCh09N015Test.testGetChar", 't', getChar("Metallica", 2));
    }
}
