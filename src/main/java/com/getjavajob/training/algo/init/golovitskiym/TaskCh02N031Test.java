package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh02N031.*;
import static com.getjavajob.training.algo.util.Assert.*;

public class TaskCh02N031Test {
    public static void main(String[] args) {
        testFindX();
    }

    private static void testFindX() {
        assertEquals("TaskCh02N031Test.testFindX", 132, findX(123));
    }
}
