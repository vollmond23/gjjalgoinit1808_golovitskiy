package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh09N022.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N022Test {
    public static void main(String[] args) {
        testGetHalfOfWord();
    }

    private static void testGetHalfOfWord() {
        assertEquals("TaskCh09N022Test.testGetHalfOfWord", "Ar", getHalfOfWord("Aria"));
    }
}
