package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh05N038.*;
import static com.getjavajob.training.algo.util.Assert.*;

public class TaskCh05N038Test {
    public static void main(String[] args) {
        testGetFullDistance();
        testGetDistanceFromHome();
    }

    private static void testGetFullDistance() {
        assertEquals("TaskCh05N038Test.testGetFullDistance", 1.5, getFullDistance(2));
    }

    private static void testGetDistanceFromHome() {
        assertEquals("TaskCh05N038Test.testGetDistanceFromHome", 0.5, getDistanceFromHome(2));
    }
}
