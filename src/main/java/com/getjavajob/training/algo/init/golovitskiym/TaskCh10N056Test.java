package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh10N056.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N056Test {
    public static void main(String[] args) {
        testIsSimpleTrue();
        testIsSimpleFalse();
    }

    private static void testIsSimpleTrue() {
        assertEquals("TaskCh10N056Test.testIsSimpleTrue", true, isSimple(17, 2));
    }

    private static void testIsSimpleFalse() {
        assertEquals("TaskCh10N056Test.testIsSimpleFalse", false, isSimple(16, 2));
    }
}
