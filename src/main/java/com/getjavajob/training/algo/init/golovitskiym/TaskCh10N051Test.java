package com.getjavajob.training.algo.init.golovitskiym;

import java.io.File;
import java.io.IOException;

import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N051Test {
    public static void main(String[] args) throws IOException {
        testFiles();
    }

    private static void testFiles() throws IOException {
        File expectedOutput = new File("src/com/getjavajob/training/algo/init/golovitskiym/TaskCh10N051ExpectedOutputs.txt");
        File actualOutput = new File("src/com/getjavajob/training/algo/init/golovitskiym/TaskCh10N051ActualOutputs.txt");
        assertEquals("TaskCh10N051Test.testFiles", expectedOutput, actualOutput);
    }
}
