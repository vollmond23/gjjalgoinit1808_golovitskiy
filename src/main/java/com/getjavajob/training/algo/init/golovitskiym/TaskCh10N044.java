package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.getIntegerFromConsole;

public class TaskCh10N044 {
    public static void main(String[] args) {
        System.out.print("Input a number: ");
        int number = getIntegerFromConsole();
        System.out.println("The digital root is " + getDigitalRoot(number));
    }

    static int addDigitsTogether(int number) {
        if (number < 10) {
            return number;
        }
        return number % 10 + addDigitsTogether(number / 10);
    }

    static int getDigitalRoot(int number) {
        if (number < 10) {
            return number;
        }
        return getDigitalRoot(addDigitsTogether(number));
    }
}
