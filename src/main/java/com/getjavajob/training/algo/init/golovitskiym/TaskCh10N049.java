package com.getjavajob.training.algo.init.golovitskiym;

import java.util.Random;

import static com.getjavajob.training.algo.util.DataFromConsole.getIntegerFromConsole;

public class TaskCh10N049 {
    public static void main(String[] args) {
        System.out.print("Input the size of array: ");
        int arraySize = getIntegerFromConsole();
        int[] array = new int[arraySize];
        Random randomNumber = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = randomNumber.nextInt(100);
        }
        System.out.print("You got an array: ");
        for (int member : array) {
            System.out.print(member + " ");
        }
        System.out.println();
        System.out.println("Index of max number is " + findMaxNumberIndex(array, 0));
    }

    static int findMaxNumberIndex(int[] array, int currentIndex) {
        if (array.length > currentIndex) {
            int nextMemberIndex = findMaxNumberIndex(array, currentIndex + 1);
            return array[currentIndex] > array[nextMemberIndex] ? currentIndex : nextMemberIndex;
        } else {
            return 0;
        }
    }
}
