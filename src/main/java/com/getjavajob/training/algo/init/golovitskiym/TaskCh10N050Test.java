package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh10N050.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N050Test {
    public static void main(String[] args) {
        testFindAckermannSolution();
    }

    private static void testFindAckermannSolution() {
        assertEquals("TaskCh10N050Test.testFindAckermannSolution", 5, findAckermannSolution(1, 3));
    }
}
