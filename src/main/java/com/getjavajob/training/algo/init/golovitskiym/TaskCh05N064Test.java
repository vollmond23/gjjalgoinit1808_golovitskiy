package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh05N064.*;
import static com.getjavajob.training.algo.util.Assert.*;

public class TaskCh05N064Test {
    public static void main(String[] args) {
        testCalculatePopulationDensity();
    }

    private static void testCalculatePopulationDensity() {
        int[][] actualArray = {
                {100, 100},
                {200, 200},
                {300, 300},
                {400, 400},
                {500, 500},
                {600, 600},
                {700, 700},
                {800, 800},
                {900, 900},
                {1000, 1000},
                {1100, 1100},
                {1200, 1200},
        };

        assertEquals("TaskCh05N064Test.testCalculatePopulationDensity", 1.0, calculatePopulationDensity(actualArray));
    }
}
