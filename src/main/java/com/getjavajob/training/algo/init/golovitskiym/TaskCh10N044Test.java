package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh10N044.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N044Test {
    public static void main(String[] args) {
        testGetDigitalRoot();
    }

    private static void testGetDigitalRoot() {
        assertEquals("TaskCh10N044Test.testGetDigitalRoot", 9, getDigitalRoot(123456789));
    }
}
