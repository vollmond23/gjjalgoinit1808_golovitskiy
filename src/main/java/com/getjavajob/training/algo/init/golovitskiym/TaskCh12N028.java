package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.getIntegerFromConsole;

public class TaskCh12N028 {
    public static void main(String[] args) {
        System.out.print("Input the size of array: ");
        int arraySize = getOddIntegerFromConsole();
        print2DArray(fillUpArray(arraySize));
    }

    static int[][] fillUpArray(int arraySize) {
        int[][] resultArray = new int[arraySize][arraySize];
        int i = 0, j = 0, k = 1;
        int limitMin = 0, limitMax = arraySize - 1;
        while (i != arraySize / 2 && j != arraySize / 2) {
            if (i != 0) {
                limitMax--;
            }
            while (j < limitMax) {
                resultArray[i][j++] = k++;
            }
            while (i < limitMax) {
                resultArray[i++][j] = k++;
            }
            if (i != arraySize - 1) {
                limitMin++;
            }
            while (j > limitMin) {
                resultArray[i][j--] = k++;
            }
            while (i > limitMin) {
                resultArray[i--][j] = k++;
            }
            i++;
            j++;
        }
        resultArray[i][j] = arraySize * arraySize;
        return resultArray;
    }

    static private int getOddIntegerFromConsole() {
        int number;
        while (true) {
            number = getIntegerFromConsole();
            if (number <= 1 && number % 2 == 0) {
                System.out.println("Number should be odd and bigger then 1.");
            } else {
                break;
            }
        }
        return number;
    }

    static private void print2DArray(int[][] array) {
        for (int[] rows : array) {
            for (int element : rows) {
                System.out.print(element + "\t");
            }
            System.out.println();
        }
    }
}
