package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.getIntegerFromConsole;
import static java.lang.Math.*;

public class TaskCh10N052 {
    public static void main(String[] args) {
        System.out.print("Input a number: ");
        int number = getIntegerFromConsole();
        System.out.println("Backward number: " + getDigitsBackwards(number));
    }

    static int getDigitsBackwards(int number) {
        if (number < 10) {
            return number % 10;
        }
        return number % 10 * (int) pow(10, (int) log10(number)) + getDigitsBackwards(number / 10);
    }
}
