package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.getIntegerFromConsole;

public class TaskCh10N046 {
    public static void main(String[] args) {
        System.out.print("Input the first member of geometric progression: ");
        int firstMember = getIntegerFromConsole();
        System.out.print("Input the denominator of geometric progression: ");
        int denominator = getIntegerFromConsole();
        System.out.print("Input the number of random member of geometric progression: ");
        int index = getIntegerFromConsole();
        System.out.println("The member is " + findMember(firstMember, denominator, index));
        System.out.println("The sum of members is " + findSumOfMembers(firstMember, denominator, index));
    }

    static int findMember(int firstMember, int denominator, int index) {
        int nextMember = firstMember * denominator;
        if (index == 1) {
            return firstMember;
        }
        return findMember(nextMember, denominator, index - 1);
    }

    static int findSumOfMembers(int firstMember, int denominator, int index) {
        int nextMember = firstMember * denominator;
        if (index == 1) {
            return firstMember;
        }
        return firstMember + findSumOfMembers(nextMember, denominator, index - 1);
    }
}
