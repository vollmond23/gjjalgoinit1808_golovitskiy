package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.getIntegerFromConsole;
import static com.getjavajob.training.algo.util.DataFromConsole.getIntegerFromConsoleInRange;

public class TaskCh10N055 {
    public static void main(String[] args) {
        System.out.print("Input a number system: ");
        int numberSystem = getIntegerFromConsoleInRange(2, 16);
        System.out.print("Input a number: ");
        int number = getIntegerFromConsole();
        char[] symbols = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        System.out.println("Number in new number system: " + convertToNumberSystem(symbols, numberSystem, number));
    }

    static StringBuilder convertToNumberSystem(char[] symbols, int numberSystem, int number) {
        StringBuilder resultNumber = new StringBuilder();
        int div = number / numberSystem;
        if (div == 0) {
            return resultNumber.append(symbols[number]);
        } else {
            resultNumber.append(convertToNumberSystem(symbols, numberSystem, div));
            return resultNumber.append(symbols[number % numberSystem]);
        }
    }
}
