package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh06N008.*;
import static com.getjavajob.training.algo.util.Assert.*;

public class TaskCh06N008Test {
    public static void main(String[] args) {
        testGetNumbersLessThen();
    }

    private static void testGetNumbersLessThen() {
        int[] expectedArray = {1, 4, 9, 16, 25, 36, 49, 64, 81, 100, 121};
        assertEquals("TaskCh06N008Test.testGetNumbersLessThen", expectedArray, getNumbersLessThen(122));
    }
}
