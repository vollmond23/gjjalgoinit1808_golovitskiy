package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh09N185.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N185Test {
    public static void main(String[] args) {
        testCheckBracesWithoutExtraBraces();
        testCheckBracesWithExtraLeftBraces();
        testCheckBracesWithExtraRightBraces();
    }

    private static void testCheckBracesWithoutExtraBraces() {
        assertEquals("TaskCh09N185Test.testCheckBracesWithoutExtraBraces", "Yes", checkBraces("(1+(2+3)+(4+5))+6"));
    }

    private static void testCheckBracesWithExtraLeftBraces() {
        assertEquals("TaskCh09N185Test.testCheckBracesWithExtraLeftBraces", "No. There are 1 extra left braces", checkBraces("(1+(2+3)+(4+5))+(6"));
    }

    private static void testCheckBracesWithExtraRightBraces() {
        assertEquals("TaskCh09N185Test.testCheckBracesWithExtraRightBraces", "No. There is extra right brace on position 17", checkBraces("(1+(2+3)+(4+5))+6)"));
    }
}
