package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh09N017.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N017Test {
    public static void main(String[] args) {
        testCheckCharsEquals();
        testCheckCharsNotEquals();
    }

    private static void testCheckCharsEquals() {
        assertEquals("TaskCh09N017Test.testCheckCharsEquals", "Chars equal.", checkCharsEquals("Aria"));
    }

    private static void testCheckCharsNotEquals() {
        assertEquals("TaskCh09N017Test.testCheckCharsNotEquals", "Chars not equal.", checkCharsEquals("Metallica"));
    }
}
