package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh12N023.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N023Test {
    public static void main(String[] args) {
        testFillUpArrayTaskA();
        testFillUpArrayTaskB();
        testFillUpArrayTaskC();
    }

    private static void testFillUpArrayTaskA() {
        int[][] expectedArray = {
                {1, 0, 0, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 1, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 1, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 0, 0, 1}};
        assertEquals("TaskCh12N023Test.testFillUpArrayTaskA", expectedArray, fillUpArrayTaskA(7));
    }

    private static void testFillUpArrayTaskB() {
        int[][] expectedArray = {
                {1, 0, 0, 1, 0, 0, 1},
                {0, 1, 0, 1, 0, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {1, 1, 1, 1, 1, 1, 1},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 0, 1, 0, 1, 0},
                {1, 0, 0, 1, 0, 0, 1}};
        assertEquals("TaskCh12N023Test.testFillUpArrayTaskB", expectedArray, fillUpArrayTaskB(7));
    }

    private static void testFillUpArrayTaskC() {
        int[][] expectedArray = {
                {1, 1, 1, 1, 1, 1, 1},
                {0, 1, 1, 1, 1, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 1, 1, 1, 1, 0},
                {1, 1, 1, 1, 1, 1, 1}};
        assertEquals("TaskCh12N023Test.testFillUpArrayTaskC", expectedArray, fillUpArrayTaskC(7));
    }
}
