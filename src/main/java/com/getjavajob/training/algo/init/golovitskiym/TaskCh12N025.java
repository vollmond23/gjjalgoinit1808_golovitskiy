package com.getjavajob.training.algo.init.golovitskiym;

public class TaskCh12N025 {
    public static void main(String[] args) {
        print2DArray(fillUpArrayTaskA());
        System.out.println();
        print2DArray(fillUpArrayTaskB());
        System.out.println();
        print2DArray(fillUpArrayTaskV());
        System.out.println();
        print2DArray(fillUpArrayTaskG());
        System.out.println();
        print2DArray(fillUpArrayTaskD());
        System.out.println();
        print2DArray(fillUpArrayTaskE());
        System.out.println();
        print2DArray(fillUpArrayTaskZh());
        System.out.println();
        print2DArray(fillUpArrayTaskZ());
        System.out.println();
        print2DArray(fillUpArrayTaskI());
        System.out.println();
        print2DArray(fillUpArrayTaskK());
        System.out.println();
        print2DArray(fillUpArrayTaskL());
        System.out.println();
        print2DArray(fillUpArrayTaskM());
        System.out.println();
        print2DArray(fillUpArrayTaskN());
        System.out.println();
        print2DArray(fillUpArrayTaskO());
        System.out.println();
        print2DArray(fillUpArrayTaskP());
        System.out.println();
        print2DArray(fillUpArrayTaskR());
    }

    static private void print2DArray(int[][] array) {
        for (int[] rows : array) {
            for (int element : rows) {
                System.out.print(element + "\t");
            }
            System.out.println();
        }
    }

    static int[][] fillUpArrayTaskA() {
        int[][] resultArray = new int[12][10];
        int k = 1;
        for (int i = 0; i < resultArray.length; i++) {
            for (int j = 0; j < resultArray[i].length; j++) {
                resultArray[i][j] = k++;
            }
        }
        return resultArray;
    }

    static int[][] fillUpArrayTaskB() {
        int[][] resultArray = new int[12][10];
        int k = 1;
        for (int j = 0; j < 10; j++) {
            for (int i = 0; i < resultArray.length; i++) {
                resultArray[i][j] = k++;
            }
        }
        return resultArray;
    }

    static int[][] fillUpArrayTaskV() {
        int[][] resultArray = new int[12][10];
        int k = 1;
        for (int i = 0; i < resultArray.length; i++) {
            for (int j = resultArray[i].length - 1; j >= 0; j--) {
                resultArray[i][j] = k++;
            }
        }
        return resultArray;
    }

    static int[][] fillUpArrayTaskG() {
        int[][] resultArray = new int[12][10];
        int k = 1;
        for (int j = 0; j < 10; j++) {
            for (int i = resultArray.length - 1; i >= 0; i--) {
                resultArray[i][j] = k++;
            }
        }
        return resultArray;
    }

    static int[][] fillUpArrayTaskD() {
        int[][] resultArray = new int[10][12];
        int k = 1;
        for (int i = 0; i < resultArray.length; i++) {
            if (i % 2 == 0) {
                for (int j = 0; j < resultArray[i].length; j++) {
                    resultArray[i][j] = k++;
                }
            } else {
                for (int j = resultArray[i].length - 1; j >= 0; j--) {
                    resultArray[i][j] = k++;
                }
            }
        }
        return resultArray;
    }

    static int[][] fillUpArrayTaskE() {
        int[][] resultArray = new int[12][10];
        int k = 1;
        for (int j = 0; j < 10; j++) {
            if (j % 2 == 0) {
                for (int i = 0; i < resultArray.length; i++) {
                    resultArray[i][j] = k++;
                }
            } else {
                for (int i = resultArray.length - 1; i >= 0; i--) {
                    resultArray[i][j] = k++;
                }
            }
        }
        return resultArray;
    }

    static int[][] fillUpArrayTaskZh() {
        int[][] resultArray = new int[12][10];
        int k = 1;
        for (int i = resultArray.length - 1; i >= 0; i--) {
            for (int j = 0; j < resultArray[i].length; j++) {
                resultArray[i][j] = k++;
            }
        }
        return resultArray;
    }

    static int[][] fillUpArrayTaskZ() {
        int[][] resultArray = new int[12][10];
        int k = 1;
        for (int j = 9; j >= 0; j--) {
            for (int i = 0; i < resultArray.length; i++) {
                resultArray[i][j] = k++;
            }
        }
        return resultArray;
    }

    static int[][] fillUpArrayTaskI() {
        int[][] resultArray = new int[12][10];
        int k = 1;
        for (int i = resultArray.length - 1; i >= 0; i--) {
            for (int j = resultArray[i].length - 1; j >= 0; j--) {
                resultArray[i][j] = k++;
            }
        }
        return resultArray;
    }

    static int[][] fillUpArrayTaskK() {
        int[][] resultArray = new int[12][10];
        int k = 1;
        for (int j = 9; j >= 0; j--) {
            for (int i = resultArray.length - 1; i >= 0; i--) {
                resultArray[i][j] = k++;
            }
        }
        return resultArray;
    }

    static int[][] fillUpArrayTaskL() {
        int[][] resultArray = new int[12][10];
        int k = 1;
        for (int i = resultArray.length - 1; i >= 0; i--) {
            if (i % 2 == 0) {
                for (int j = resultArray[i].length - 1; j >= 0; j--) {
                    resultArray[i][j] = k++;
                }
            } else {
                for (int j = 0; j < resultArray[i].length; j++) {
                    resultArray[i][j] = k++;
                }
            }
        }
        return resultArray;
    }

    static int[][] fillUpArrayTaskM() {
        int[][] resultArray = new int[12][10];
        int k = 1;
        for (int i = 0; i < resultArray.length; i++) {
            if (i % 2 == 0) {
                for (int j = resultArray[i].length - 1; j >= 0; j--) {
                    resultArray[i][j] = k++;
                }
            } else {
                for (int j = 0; j < resultArray[i].length; j++) {
                    resultArray[i][j] = k++;
                }
            }
        }
        return resultArray;
    }

    static int[][] fillUpArrayTaskN() {
        int[][] resultArray = new int[12][10];
        int k = 1;
        for (int j = 9; j >= 0; j--) {
            if (j % 2 == 0) {
                for (int i = resultArray.length - 1; i >= 0; i--) {
                    resultArray[i][j] = k++;
                }
            } else {
                for (int i = 0; i < resultArray.length; i++) {
                    resultArray[i][j] = k++;
                }
            }
        }
        return resultArray;
    }

    static int[][] fillUpArrayTaskO() {
        int[][] resultArray = new int[12][10];
        int k = 1;
        for (int j = 0; j < 10; j++) {
            if (j % 2 == 0) {
                for (int i = resultArray.length - 1; i >= 0; i--) {
                    resultArray[i][j] = k++;
                }
            } else {
                for (int i = 0; i < resultArray.length; i++) {
                    resultArray[i][j] = k++;
                }
            }
        }
        return resultArray;
    }

    static int[][] fillUpArrayTaskP() {
        int[][] resultArray = new int[12][10];
        int k = 1;
        for (int i = resultArray.length - 1; i >= 0; i--) {
            if (i % 2 == 0) {
                for (int j = 0; j < resultArray[i].length; j++) {
                    resultArray[i][j] = k++;
                }
            } else {
                for (int j = resultArray[i].length - 1; j >= 0; j--) {
                    resultArray[i][j] = k++;
                }
            }
        }
        return resultArray;
    }

    static int[][] fillUpArrayTaskR() {
        int[][] resultArray = new int[12][10];
        int k = 1;
        for (int j = 9; j >= 0; j--) {
            if (j % 2 == 0) {
                for (int i = 0; i < resultArray.length; i++) {
                    resultArray[i][j] = k++;
                }
            } else {
                for (int i = resultArray.length - 1; i >= 0; i--) {
                    resultArray[i][j] = k++;
                }
            }
        }
        return resultArray;
    }
}
