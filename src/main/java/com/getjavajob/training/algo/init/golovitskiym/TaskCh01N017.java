package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.*;

import static java.lang.Math.*;

public class TaskCh01N017 {
    public static void main(String[] args) {
        System.out.println("Input X:");
        int inputX = getIntegerFromConsole();
        System.out.println("Input A:");
        int inputA = getIntegerFromConsole();
        System.out.println("Input B:");
        int inputB = getIntegerFromConsole();
        System.out.println("Input C:");
        int inputC = getIntegerFromConsole();
        System.out.println("Solution of subtask O: " + getResultO(inputX));
        System.out.println("Solution of subtask P: " + getResultP(inputX, inputA, inputB, inputC));
        System.out.println("Solution of subtask R: " + getResultR(inputX));
        System.out.println("Solution of subtask S: " + getResultS(inputX));
    }

    private static double getResultO(int x) {
        return sqrt(1 - pow(sin(x), 2.0));
    }

    private static double getResultP(int x, int a, int b, int c) {
        return 1.0 / sqrt(a * x * x + b * x + c);
    }

    private static double getResultR(int x) {
        return (sqrt(x + 1) + sqrt(x - 1)) / 2 * sqrt(x);
    }

    private static double getResultS(int x) {
        return abs(x) + abs(x + 1);
    }
}
