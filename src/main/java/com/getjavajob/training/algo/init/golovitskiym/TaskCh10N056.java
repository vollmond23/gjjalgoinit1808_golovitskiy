package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.getIntegerFromConsoleInRange;

public class TaskCh10N056 {
    public static void main(String[] args) {
        System.out.print("Input a number: ");
        int number = getIntegerFromConsoleInRange(2, Integer.MAX_VALUE);
        System.out.println(isSimple(number, 2) ? "Number is simple" : "Number is not simple");
    }

    static boolean isSimple(int number, int divider) {
        if (number == 2) {
            return true;
        } else if (number % divider == 0) {
            return false;
        } else if (divider < number / 2) {
            return isSimple(number, divider + 1);
        } else {
            return true;
        }
    }
}
