package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.*;

public class TaskCh06N008 {
    public static void main(String[] args) {
        System.out.print("Input a number: ");
        int limitNumber = getIntegerFromConsole();
        System.out.print("Result numbers: ");
        for (int number : getNumbersLessThen(limitNumber)) {
            System.out.print(number + "\t");
        }
    }

    static int[] getNumbersLessThen(int limitNumber) {
        int counter = 0;
        while (counter * counter < limitNumber) {
            counter++;
        }
        int[] resultNumbers = new int[counter - 1];
        for (int i = 0; i < resultNumbers.length; i++) {
            resultNumbers[i] = (i + 1) * (i + 1);
        }
        return resultNumbers;
    }
}
