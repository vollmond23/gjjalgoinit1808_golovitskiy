package com.getjavajob.training.algo.init.golovitskiym;

import java.util.Random;

import static com.getjavajob.training.algo.util.DataFromConsole.getIntegerFromConsole;

public class TaskCh10N053 {
    public static void main(String[] args) {
        System.out.print("Input the size of array: ");
        int arraySize = getIntegerFromConsole();
        int[] array = new int[arraySize];
        Random randomNumber = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = randomNumber.nextInt(100);
        }
        System.out.print("You got an array: ");
        printArrayInline(array);
        int[] backwardArray = flipArrayBackward(array, 0);
        System.out.print("Backward array: ");
        printArrayInline(backwardArray);
    }

    static int[] flipArrayBackward(int[] array, int currentIndex) {
        if (currentIndex >= array.length / 2) {
            return array;
        } else {
            int buffer = array[currentIndex];
            array[currentIndex] = array[array.length - 1 - currentIndex];
            array[array.length - 1 - currentIndex] = buffer;
            return flipArrayBackward(array, currentIndex + 1);
        }
    }

    static void printArrayInline(int[] array) {
        for (int member : array) {
            System.out.print(member + " ");
        }
        System.out.println();
    }
}
