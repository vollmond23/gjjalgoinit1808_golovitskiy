package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh10N048.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N048Test {
    public static void main(String[] args) {
        testFindMaxNumber();
    }

    private static void testFindMaxNumber() {
        assertEquals("TaskCh10N048Test.testFindMaxNumber", 78, findMaxNumber(new int[]{2, 8, 3, 0, 12, 4, 78, 2, 23}, 0));
    }
}
