package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.*;

public class TaskCh02N031 {
    public static void main(String[] args) {
        System.out.println("Input a number between 100 and 999 (inclusive):");
        int number = getIntegerFromConsoleInRange(100, 999);
        System.out.println("X was found: " + findX(number));
    }

    static int findX(int number) {
        int ones = number % 10;
        int tens = number % 100 / 10;
        int hundreds = number % 1000 / 100;
        return hundreds * 100 + ones * 10 + tens;
    }
}
