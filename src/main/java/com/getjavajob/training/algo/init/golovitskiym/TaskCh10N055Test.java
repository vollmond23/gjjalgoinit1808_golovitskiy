package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh10N055.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N055Test {
    public static void main(String[] args) {
        testConvertToNumberSystem();
    }

    private static void testConvertToNumberSystem() {
        char[] symbols = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        assertEquals("TaskCh10N055Test.testConvertToNumberSystem", "4D2", convertToNumberSystem(symbols, 16, 1234).toString());
    }
}
