package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.*;

public class TaskCh04N106 {
    public static void main(String[] args) {
        System.out.print("Input the number of month: ");
        int numberOfTheMonth = getIntegerFromConsoleInRange(1, 12);
        System.out.println(getSeason(numberOfTheMonth));
    }

    static String getSeason(int numberOfTheMonth) {
        String season;
        switch (numberOfTheMonth) {
            case 12:
            case 1:
            case 2:
                season = "Winter";
                break;
            case 3:
            case 4:
            case 5:
                season = "Spring";
                break;
            case 6:
            case 7:
            case 8:
                season = "Summer";
                break;
            case 9:
            case 10:
            case 11:
                season = "Autumn";
                break;
            default:
                season = "unknown";
                break;

        }
        return season;
    }
}
