package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh10N046.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N046Test {
    public static void main(String[] args) {
        testFindMember();
        testFindSumOfMembers();
    }

    private static void testFindMember() {
        assertEquals("TaskCh10N046Test.testFindMember", 4, findMember(1, 2, 3));
    }

    private static void testFindSumOfMembers() {
        assertEquals("TaskCh10N046Test.testFindSumOfMembers", 7, findSumOfMembers(1, 2, 3));
    }
}
