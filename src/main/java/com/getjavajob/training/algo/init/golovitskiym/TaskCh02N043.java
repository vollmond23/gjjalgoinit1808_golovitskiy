package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.*;

public class TaskCh02N043 {
    public static void main(String[] args) {
        System.out.println("Enter A:");
        int inputA = getIntegerFromConsole();
        System.out.println("Enter B:");
        int inputB = getIntegerFromConsole();
        System.out.println("Solution: " + returnSolution(inputA, inputB));
    }

    static int returnSolution(int a, int b) {
        return a % b == 0 || b % a == 0 ? 1 : 0;
    }
}
