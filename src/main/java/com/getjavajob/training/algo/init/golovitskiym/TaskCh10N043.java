package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.*;

public class TaskCh10N043 {
    public static void main(String[] args) {
        System.out.print("Input a number: ");
        int number = getIntegerFromConsole();
        System.out.println("The sum of digits is " + addDigitsTogether(number));
        System.out.println("The count of digits is " + countDigits(number));
    }

    static int addDigitsTogether(int number) {
        if (number < 10) {
            return number;
        }
        return number % 10 + addDigitsTogether(number / 10);
    }

    static int countDigits(int number) {
        if (number < 10) {
            return 1;
        }
        return 1 + countDigits(number / 10);
    }
}
