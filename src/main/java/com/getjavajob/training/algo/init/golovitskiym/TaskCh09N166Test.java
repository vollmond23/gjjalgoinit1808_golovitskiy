package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh09N166.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N166Test {
    public static void main(String[] args) {
        testSwapFirstAndLastWords();
    }

    private static void testSwapFirstAndLastWords() {
        assertEquals("TaskCh09N166Test.testSwapFirstAndLastWords", "phrase a Input", swapFirstAndLastWords("Input a phrase"));
    }
}
