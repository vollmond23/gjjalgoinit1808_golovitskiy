package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh04N015.*;
import static com.getjavajob.training.algo.util.Assert.*;

public class TaskCh04N015Test {
    public static void main(String[] args) {
        testGetAge1();
        testGetAge2();
        testGetAge3();
    }

    private static void testGetAge1() {
        assertEquals("TaskCh04N015Test.testGetAge1", 29, getAge(6, 1985, 12, 2014));
    }

    private static void testGetAge2() {
        assertEquals("TaskCh04N015Test.testGetAge2", 28, getAge(6, 1985, 5, 2014));
    }

    private static void testGetAge3() {
        assertEquals("TaskCh04N015Test.testGetAge3", 29, getAge(6, 1985, 6, 2014));
    }
}
