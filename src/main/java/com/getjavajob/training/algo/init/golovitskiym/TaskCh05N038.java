package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.*;

public class TaskCh05N038 {
    public static void main(String[] args) {
        System.out.print("Input the Strange Husband's number of steps: ");
        int steps = getIntegerFromConsole();
        System.out.println("Full distance: " + getFullDistance(steps));
        System.out.println("Distance from home: " + getDistanceFromHome(steps));
    }

    static double getDistanceFromHome(int steps) {
        double distanceFromHome = 0.0;
        double stepDistance;
        for (int i = 1; i <= steps; i++) {
            stepDistance = (i % 2 != 0) ? 1.0 / i : - 1.0 / i;
            distanceFromHome += stepDistance;
        }
        return distanceFromHome;
    }

    static double getFullDistance(int steps) {
        double distance = 0;
        for (int i = 1; i <= steps; i++) {
            distance += 1.0 / i;
        }
        return distance;
    }
}