package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.*;

public class TaskCh09N185 {
    public static void main(String[] args) {
        System.out.print("Input an arithmetic expression: ");
        String expression = getStringFromConsole();
        System.out.println(checkBraces(expression));
    }

    static String checkBraces(String expression) {
        char[] chars = expression.toCharArray();
        int bracesCounter = 0;
        int extraBraceIndex = -1;
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == '(') {
                bracesCounter++;
            } else if (chars[i] == ')') {
                bracesCounter--;
            }
            if (bracesCounter < 0) {
                extraBraceIndex = i;
                break;
            }
        }
        String answer = "";
        if (extraBraceIndex != -1) {
            answer = "No. There is extra right brace on position " + extraBraceIndex;
        } else if (bracesCounter > 0) {
            answer = "No. There are " + bracesCounter + " extra left braces";
        } else {
            answer = "Yes";
        }
        return answer;
    }
}
