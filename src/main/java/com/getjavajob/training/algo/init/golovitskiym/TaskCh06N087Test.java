package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.Assert.*;

public class TaskCh06N087Test {
    public static void main(String[] args) {
        testScore();
        testResult();
    }

    private static void testScore() {
        String expectedString = "Score: Bulls 3 - Cows 2";
        Game game = new Game();
        game.setTeam1Name("Bulls");
        game.setTeam2Name("Cows");
        game.setTeam1Score(3);
        game.setTeam2Score(2);
        assertEquals("TaskCh06N087Test.testScore", expectedString, game.score());
    }

    private static void testResult() {
        String expectedString = "The winner is Bulls";
        Game game = new Game();
        game.setTeam1Name("Bulls");
        game.setTeam2Name("Cows");
        game.setTeam1Score(3);
        game.setTeam2Score(2);
        assertEquals("TaskCh06N087Test.testResult", expectedString, game.result());
    }
}
