package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh10N042.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N042Test {
    public static void main(String[] args) {
        testRaiseNumberToPower();
    }

    private static void testRaiseNumberToPower() {
        assertEquals("TaskCh10N042Test.testRaiseNumberToPower", 1.21, raiseNumberToPower(1.1, 2));
    }
}
