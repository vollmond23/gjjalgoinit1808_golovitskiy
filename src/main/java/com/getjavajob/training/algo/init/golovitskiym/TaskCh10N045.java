package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.getIntegerFromConsole;

public class TaskCh10N045 {
    public static void main(String[] args) {
        System.out.print("Input the first member of arithmetic progression: ");
        int firstMember = getIntegerFromConsole();
        System.out.print("Input the difference of arithmetic progression: ");
        int difference = getIntegerFromConsole();
        System.out.print("Input the number of random member of arithmetic progression: ");
        int index = getIntegerFromConsole();
        System.out.println("The member is " + findMember(firstMember, difference, index));
        System.out.println("The sum of members is " + findSumOfMembers(firstMember, difference, index));
    }

    static int findMember(int firstMember, int difference, int index) {
        int nextMember = firstMember + difference;
        if (index == 1) {
            return firstMember;
        }
        return findMember(nextMember, difference, index - 1);
    }

    static int findSumOfMembers(int firstMember, int difference, int index) {
        int nextMember = firstMember + difference;
        if (index == 1) {
            return firstMember;
        }
        return firstMember + findSumOfMembers(nextMember, difference, index - 1);
    }
}
