package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh04N115.*;
import static com.getjavajob.training.algo.util.Assert.*;

public class TaskCh04N115Test {
    public static void main(String[] args) {
        testRedTiger();
        testYellowPig();
    }

    private static void testRedTiger() {
        assertEquals("TaskCh04N115Test.testRedTiger", "Tiger, Red", getChineseYear(1986));
    }

    private static void testYellowPig() {
        assertEquals("TaskCh04N115Test.testYellowPig", "Pig, Yellow", getChineseYear(1959));
    }
}
