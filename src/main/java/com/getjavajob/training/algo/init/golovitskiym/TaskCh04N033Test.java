package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh04N033.*;
import static com.getjavajob.training.algo.util.Assert.*;

public class TaskCh04N033Test {
    public static void main(String[] args) {
        testEven();
        testOdd();
    }

    private static void testEven() {
        assertEquals("TaskCh04N033Test.testEven", true, checkEven(124));
    }

    private static void testOdd() {
        assertEquals("TaskCh04N033Test.testOdd", false, checkEven(123));
    }
}
