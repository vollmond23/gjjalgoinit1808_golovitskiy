package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh03N029.*;
import static com.getjavajob.training.algo.util.Assert.*;

public class TaskCh03N029Test {
    public static void main(String[] args) {
        testGetResultAFirstCase();
        testGetResultASecondCase();
        testGetResultAThirdCase();
        testGetResultAForthCase();
        System.out.println();
        testGetResultBFirstCase();
        testGetResultBSecondCase();
        testGetResultBThirdCase();
        testGetResultBForthCase();
        System.out.println();
        testGetResultVFirstCase();
        testGetResultVSecondCase();
        testGetResultVThirdCase();
        testGetResultVForthCase();
        System.out.println();
        testGetResultGFirstCase();
        testGetResultGSecondCase();
        testGetResultGThirdCase();
        testGetResultGForthCase();
        testGetResultGFifthCase();
        testGetResultGSixthCase();
        testGetResultGSeventhCase();
        testGetResultGEightsCase();
        System.out.println();
        testGetResultDFirstCase();
        testGetResultDSecondCase();
        testGetResultDThirdCase();
        testGetResultDForthCase();
        testGetResultDFifthCase();
        testGetResultDSixthCase();
        testGetResultDSeventhCase();
        testGetResultDEightsCase();
        System.out.println();
        testGetResultEFirstCase();
        testGetResultESecondCase();
        testGetResultEThirdCase();
        testGetResultEForthCase();
        testGetResultEFifthCase();
        testGetResultESixthCase();
        testGetResultESeventhCase();
        testGetResultEEightsCase();
    }

    private static void testGetResultAFirstCase() {
        assertEquals("TaskCh03N029Test.testGetResultA (case 1)", true, getResultA(1, 3));
    }

    private static void testGetResultASecondCase() {
        assertEquals("TaskCh03N029Test.testGetResultA (case 2)", false, getResultA(2, 4));
    }

    private static void testGetResultAThirdCase() {
        assertEquals("TaskCh03N029Test.testGetResultA (case 3)", false, getResultA(1, 4));
    }

    private static void testGetResultAForthCase() {
        assertEquals("TaskCh03N029Test.testGetResultA (case 4)", false, getResultA(2, 3));
    }

    private static void testGetResultBFirstCase() {
        assertEquals("TaskCh03N029Test.testGetResultB (case 1)", false, getResultB(1, 3));
    }

    private static void testGetResultBSecondCase() {
        assertEquals("TaskCh03N029Test.testGetResultB (case 2)", true, getResultB(1, 23));
    }

    private static void testGetResultBThirdCase() {
        assertEquals("TaskCh03N029Test.testGetResultB (case 3)", true, getResultB(21, 3));
    }

    private static void testGetResultBForthCase() {
        assertEquals("TaskCh03N029Test.testGetResultB (case 4)", false, getResultB(21, 23));
    }

    private static void testGetResultVFirstCase() {
        assertEquals("TaskCh03N029Test.testGetResultV (case 1)", false, getResultV(1, 2));
    }

    private static void testGetResultVSecondCase() {
        assertEquals("TaskCh03N029Test.testGetResultV (case 2)", true, getResultV(0, 2));
    }

    private static void testGetResultVThirdCase() {
        assertEquals("TaskCh03N029Test.testGetResultV (case 3)", true, getResultV(1, 0));
    }

    private static void testGetResultVForthCase() {
        assertEquals("TaskCh03N029Test.testGetResultV (case 4)", true, getResultV(0, 0));
    }

    private static void testGetResultGFirstCase() {
        assertEquals("TaskCh03N029Test.testGetResultG (case 1)", true, getResultG(-1, -2, -3));
    }

    private static void testGetResultGSecondCase() {
        assertEquals("TaskCh03N029Test.testGetResultG (case 2)", false, getResultG(1, -2, -3));
    }

    private static void testGetResultGThirdCase() {
        assertEquals("TaskCh03N029Test.testGetResultG (case 3)", false, getResultG(-1, 2, -3));
    }

    private static void testGetResultGForthCase() {
        assertEquals("TaskCh03N029Test.testGetResultG (case 4)", false, getResultG(-1, -2, 3));
    }

    private static void testGetResultGFifthCase() {
        assertEquals("TaskCh03N029Test.testGetResultG (case 5)", false, getResultG(1, 2, -3));
    }

    private static void testGetResultGSixthCase() {
        assertEquals("TaskCh03N029Test.testGetResultG (case 6)", false, getResultG(1, -2, 3));
    }

    private static void testGetResultGSeventhCase() {
        assertEquals("TaskCh03N029Test.testGetResultG (case 7)", false, getResultG(-1, 2, 3));
    }

    private static void testGetResultGEightsCase() {
        assertEquals("TaskCh03N029Test.testGetResultG (case 8)", false, getResultG(1, 2, 3));
    }

    private static void testGetResultDFirstCase() {
        assertEquals("TaskCh03N029Test.testGetResultD (case 1)", false, getResultD(5, 5, 5));
    }

    private static void testGetResultDSecondCase() {
        assertEquals("TaskCh03N029Test.testGetResultD (case 1)", true, getResultD(5, 1, 1));
    }

    private static void testGetResultDThirdCase() {
        assertEquals("TaskCh03N029Test.testGetResultD (case 3)", true, getResultD(1, 5, 1));
    }

    private static void testGetResultDForthCase() {
        assertEquals("TaskCh03N029Test.testGetResultD (case 4)", true, getResultD(1, 1, 5));
    }

    private static void testGetResultDFifthCase() {
        assertEquals("TaskCh03N029Test.testGetResultD (case 5)", false, getResultD(5, 5, 1));
    }

    private static void testGetResultDSixthCase() {
        assertEquals("TaskCh03N029Test.testGetResultD (case 6)", false, getResultD(5, 1, 5));
    }

    private static void testGetResultDSeventhCase() {
        assertEquals("TaskCh03N029Test.testGetResultD (case 7)", false, getResultD(1, 5, 5));
    }

    private static void testGetResultDEightsCase() {
        assertEquals("TaskCh03N029Test.testGetResultD (case 8)", false, getResultD(1, 1, 1));
    }

    private static void testGetResultEFirstCase() {
        assertEquals("TaskCh03N029Test.testGetResultE (case 1)", true, getResultE(105, 105, 105));
    }

    private static void testGetResultESecondCase() {
        assertEquals("TaskCh03N029Test.testGetResultE (case 1)", true, getResultE(105, 1, 1));
    }

    private static void testGetResultEThirdCase() {
        assertEquals("TaskCh03N029Test.testGetResultE (case 3)", true, getResultE(1, 105, 1));
    }

    private static void testGetResultEForthCase() {
        assertEquals("TaskCh03N029Test.testGetResultE (case 4)", true, getResultE(1, 1, 105));
    }

    private static void testGetResultEFifthCase() {
        assertEquals("TaskCh03N029Test.testGetResultE (case 5)", true, getResultE(105, 105, 1));
    }

    private static void testGetResultESixthCase() {
        assertEquals("TaskCh03N029Test.testGetResultE (case 6)", true, getResultE(105, 1, 105));
    }

    private static void testGetResultESeventhCase() {
        assertEquals("TaskCh03N029Test.testGetResultE (case 7)", true, getResultE(1, 105, 105));
    }

    private static void testGetResultEEightsCase() {
        assertEquals("TaskCh03N029Test.testGetResultE (case 8)", false, getResultE(1, 1, 1));
    }
}
