package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh04N036.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N036Test {
    public static void main(String[] args) {
        testRed();
        testGreen();
    }

    private static void testRed() {
        assertEquals("TaskCh04N036Test.testRed", "red", getColor(3.0));
    }

    private static void testGreen() {
        assertEquals("TaskCh04N036Test.testGreen", "green", getColor(5.0));
    }
}
