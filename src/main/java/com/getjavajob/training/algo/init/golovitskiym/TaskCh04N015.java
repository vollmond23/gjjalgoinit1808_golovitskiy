package com.getjavajob.training.algo.init.golovitskiym;

import java.util.Calendar;
import java.util.GregorianCalendar;

import static com.getjavajob.training.algo.util.DataFromConsole.*;

public class TaskCh04N015 {
    private static final long SECONDS_IN_YEAR;

    static {
        SECONDS_IN_YEAR = 60 * 60 * 24 * 365;
    }

    public static void main(String[] args) {
        System.out.println("Input a birthday month (number):");
        int birthdayMonth = getIntegerFromConsoleInRange(1, 12);
        System.out.println("Input a birthday year:");
        int birthdayYear = getIntegerFromConsole();
        System.out.println("Input today month (number):");
        int todayMonth = getIntegerFromConsoleInRange(1, 12);
        System.out.println("Input today year:");
        int todayYear = getIntegerFromConsole();
        System.out.println("Age is " + getAge(birthdayMonth, birthdayYear, todayMonth, todayYear) + " years.");
    }

    static int getAge(int birthdayMonth, int birthdayYear, int todayMonth, int todayYear) {
        GregorianCalendar birthday = new GregorianCalendar();
        birthday.set(Calendar.MONTH, birthdayMonth);
        birthday.set(Calendar.YEAR, birthdayYear);
        GregorianCalendar today = new GregorianCalendar();
        today.set(Calendar.MONTH, todayMonth);
        today.set(Calendar.YEAR, todayYear);
        long differenceInSeconds = (today.getTimeInMillis() - birthday.getTimeInMillis()) / 1000;
        return (int) (differenceInSeconds / SECONDS_IN_YEAR);
    }
}
