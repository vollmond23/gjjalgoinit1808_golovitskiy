package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh10N049.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N049Test {
    public static void main(String[] args) {
        testFindMaxNumberIndex();
    }

    private static void testFindMaxNumberIndex() {
        assertEquals("TaskCh10N049Test.testFindMaxNumberIndex", 6, findMaxNumberIndex(new int[]{2, 8, 3, 0, 12, 4, 78, 2, 23}, 0));
    }
}
