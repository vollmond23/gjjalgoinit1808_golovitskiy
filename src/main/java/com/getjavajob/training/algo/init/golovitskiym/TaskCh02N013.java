package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.*;

public class TaskCh02N013 {
    public static void main(String[] args) {
        System.out.println("Input a number between 100 and 200:");
        int number = getIntegerFromConsoleInRange(100, 200);
        System.out.println("Reverse number: " + reverseNumber(number));
    }

    static int reverseNumber(int number) {
        int tempNumber = number;
        int rank = 100;
        int reverseNum = 0;
        while (tempNumber > 0) {
            int remainder = tempNumber % 10;
            reverseNum += remainder * rank;
            tempNumber /= 10;
            rank /= 10;
        }
        return reverseNum;
    }
}
