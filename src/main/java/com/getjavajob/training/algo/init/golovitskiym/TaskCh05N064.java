package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.*;

public class TaskCh05N064 {
    public static void main(String[] args) {
        System.out.println("Population density of the whole region is: " + calculatePopulationDensity(fillInRegionData(12)));
    }

    private static int[][] fillInRegionData(int numberOfDistricts) {
        int[][] regionAmountAndSquare = new int[numberOfDistricts][2];
        for (int i = 0; i < numberOfDistricts; i++) {
            System.out.print("Input amount of people in the district #" + (i + 1) + ": ");
            regionAmountAndSquare[i][0] = getIntegerFromConsole();
            System.out.print("Input area of the district #" + (i + 1) + ": ");
            regionAmountAndSquare[i][1] = getIntegerFromConsole();
        }
        return regionAmountAndSquare;
    }

    static double calculatePopulationDensity(int[][] regionAmountAndSquare) {
        int fullAmountOfPeople = 0;
        int fullSquareOfRegion = 0;
        for (int[] region : regionAmountAndSquare) {
            fullAmountOfPeople += region[0];
            fullSquareOfRegion += region[1];
        }
        return (double) fullAmountOfPeople / fullSquareOfRegion;
    }
}
