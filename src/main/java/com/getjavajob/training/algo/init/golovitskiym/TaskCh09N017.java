package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.*;

public class TaskCh09N017 {
    public static void main(String[] args) {
        System.out.print("Input a word: ");
        String word = getStringFromConsole();
        System.out.println(checkCharsEquals(word));
    }

    static String checkCharsEquals(String word) {
        int firstChar = word.charAt(0);
        int lastChar = word.charAt(word.length() - 1);
        return firstChar == lastChar ? "Chars equal." : "Chars not equal.";
    }
}
