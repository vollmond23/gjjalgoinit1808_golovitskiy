package com.getjavajob.training.algo.init.golovitskiym;

import java.math.BigInteger;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh10N041.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N041Test {
    public static void main(String[] args) {
        testFactorial();
    }

    private static void testFactorial() {
        assertEquals("TaskCh10N041Test.testFactorial", BigInteger.valueOf(120), factorial(5));
    }
}
