package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh10N045.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N045Test {
    public static void main(String[] args) {
        testFindMember();
        testFindSumOfMembers();
    }

    private static void testFindMember() {
        assertEquals("TaskCh10N045Test.testFindMember", 5, findMember(1, 2, 3));
    }

    private static void testFindSumOfMembers() {
        assertEquals("TaskCh10N045Test.testFindSumOfMembers", 9, findSumOfMembers(1, 2, 3));
    }
}
