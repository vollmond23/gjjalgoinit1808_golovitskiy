package com.getjavajob.training.algo.init.golovitskiym;

import java.util.Arrays;
import java.util.Random;

import static com.getjavajob.training.algo.util.DataFromConsole.getIntegerFromConsole;

public class TaskCh11N245 {
    public static void main(String[] args) {
        System.out.print("Input the size of array: ");
        int arraySize = getIntegerFromConsole();
        int[] array = new int[arraySize];
        Random randomNumber = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = randomNumber.nextInt(20) - 10;
        }
        System.out.print("You got an array: ");
        System.out.println(Arrays.toString(array));
        System.out.print("Result array: ");
        System.out.println();
        System.out.println(Arrays.toString(putNegativeAtTheBeginning(array)));
    }

    static int[] putNegativeAtTheBeginning(int[] array) {
        int[] resultArray = array.clone();
        for (int i = 0; i < array.length; i++) {
            if (array[i] < 0) {
                System.arraycopy(resultArray, 0, resultArray, 1, i);
                resultArray[0] = array[i];
            }
        }
        return resultArray;
    }
}
