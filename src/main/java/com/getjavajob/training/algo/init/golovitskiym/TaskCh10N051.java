package com.getjavajob.training.algo.init.golovitskiym;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class TaskCh10N051 {
    public static void main(String[] args) throws IOException {
        File file = new File("src/com/getjavajob/training/algo/init/golovitskiym/TaskCh10N051ActualOutputs.txt");
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write("Procedure1:\r\n");
        procedure1(fileWriter, 5);
        fileWriter.write("Procedure2:\r\n");
        procedure2(fileWriter, 5);
        fileWriter.write("Procedure3:\r\n");
        procedure3(fileWriter, 5);
        fileWriter.close();
    }

    static void procedure1(FileWriter fileWriter, int n) throws IOException {
        if (n > 0) {
            fileWriter.write(n + "\r\n");
            procedure1(fileWriter, n - 1);
        }
    }

    static void procedure2(FileWriter fileWriter, int n) throws IOException {
        if (n > 0) {
            procedure2(fileWriter, n - 1);
            fileWriter.write(n + "\r\n");
        }
    }

    static void procedure3(FileWriter fileWriter, int n) throws IOException {
        if (n > 0) {
            fileWriter.write(n + "\r\n");
            procedure3(fileWriter, n - 1);
            fileWriter.write(n + "\r\n");
        }
    }
}
