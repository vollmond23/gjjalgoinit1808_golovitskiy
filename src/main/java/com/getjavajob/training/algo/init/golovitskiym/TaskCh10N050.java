package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.getIntegerFromConsole;

public class TaskCh10N050 {
    public static void main(String[] args) {
        System.out.print("Input the N: ");
        int n = getIntegerFromConsole();
        System.out.print("Input the M: ");
        int m = getIntegerFromConsole();
        System.out.println("Ackermann solution is " + findAckermannSolution(n, m));
    }

    static int findAckermannSolution(int n, int m) {
        if (n == 0) {
            return m + 1;
        } else if (m == 0) {
            return findAckermannSolution(n - 1, 1);
        } else {
            return findAckermannSolution(n - 1, findAckermannSolution(n, m - 1));
        }
    }
}
