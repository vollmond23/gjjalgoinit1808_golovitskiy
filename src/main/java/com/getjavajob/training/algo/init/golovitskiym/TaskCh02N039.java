package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.*;

public class TaskCh02N039 {
    private static final int SECONDS_IN_HOUR = 60 * 60;
    private static final int SECONDS_IN_MINUTE = 60;
    private static final double GRADS_PER_SECOND = 360.0 / (12.0 * SECONDS_IN_HOUR);

    public static void main(String[] args) {
        System.out.println("Enter H (hours):");
        int hours = getIntegerFromConsoleInRange(0, 23);
        System.out.println("Enter M (minutes):");
        int minutes = getIntegerFromConsoleInRange(0, 59);
        System.out.println("Enter S (seconds):");
        int seconds = getIntegerFromConsoleInRange(0, 59);
        System.out.println("Angle of hour hand for " + hours + ":" + minutes + ":" + seconds + " (grads): " + getAngle(hours, minutes, seconds));
    }

    static double getAngle(int hours, int minutes, int seconds) {
        return GRADS_PER_SECOND * (hours * SECONDS_IN_HOUR + minutes * SECONDS_IN_MINUTE + seconds);
    }
}
