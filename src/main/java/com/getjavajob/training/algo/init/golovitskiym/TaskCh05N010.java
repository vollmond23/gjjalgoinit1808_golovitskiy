package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.*;

public class TaskCh05N010 {
    public static void main(String[] args) {
        System.out.print("Input actual dollar exchange rate: ");
        double rate = getDoubleFromConsole();
        printRateTable(fillInRatesTable(rate));
    }

    static double[] fillInRatesTable(double rate) {
        double[] ratesArray = new double[20];
        for (int i = 0; i < ratesArray.length; i++) {
            ratesArray[i] = (i + 1) * rate;
        }
        return ratesArray;
    }

    private static void printRateTable(double[] ratesArray) {
        System.out.println("Dollars:\tRubles:");
        for (int i = 1; i < 21; i++) {
            System.out.printf("%-4d\t\t%-10.2f%n", i, ratesArray[i - 1]);
        }
    }
}
