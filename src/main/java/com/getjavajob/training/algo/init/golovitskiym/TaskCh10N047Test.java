package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh10N047.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N047Test {
    public static void main(String[] args) {
        testFindMember();
    }

    private static void testFindMember() {
        assertEquals("TaskCh10N047Test.testFindMember", 8, findMember(6));
    }
}
