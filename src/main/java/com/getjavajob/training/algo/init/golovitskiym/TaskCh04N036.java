package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.*;

public class TaskCh04N036 {
    public static void main(String[] args) {
        System.out.print("Input a double number: ");
        double minutesFromHourBegin = getDoubleFromConsole();
        System.out.println("It is " + getColor(minutesFromHourBegin) + " color.");
    }

    static String getColor(double time) {
        return time % 5 >= 0 && time % 5 < 3 ? "green" : "red";
    }
}
