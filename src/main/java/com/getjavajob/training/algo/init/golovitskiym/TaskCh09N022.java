package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.*;

public class TaskCh09N022 {
    public static void main(String[] args) {
        System.out.print("Input a word: ");
        String word = getStringFromConsole();
        System.out.println("Half of your word: " + getHalfOfWord(word));
    }

    static String getHalfOfWord(String word) {
        if (word.length() % 2 != 0) {
            return word + " with odd length";
        } else {
            int halfIndex = word.length() / 2;
            return word.substring(0, halfIndex);
        }
    }
}
