package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh12N063.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N063Test {
    public static void main(String[] args) {
        testGetStudentsCountsAverage();
    }

    private static void testGetStudentsCountsAverage() {
        int[][] initialArray = {
                {30, 30, 30, 30},
                {31, 31, 30, 30},
                {30, 30, 31, 31},
                {29, 30, 30, 31},
                {30, 30, 30, 31},
                {25, 25, 25, 25},
                {26, 26, 28, 28},
                {30, 30, 30, 30},
                {30, 30, 30, 30},
                {30, 30, 30, 30},
                {30, 30, 30, 30}};
        double[] expectedArray = {30.0, 30.5, 30.5, 30.0, 30.25, 25.0, 27.0, 30.0, 30.0, 30.0, 30.0};
        assertEquals("TaskCh12N063Test.testGetStudentsCountsAverage", expectedArray, getStudentsCountsAverage(initialArray));
    }
}
