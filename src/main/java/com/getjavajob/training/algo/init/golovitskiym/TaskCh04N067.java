package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.*;

public class TaskCh04N067 {
    public static void main(String[] args) {
        System.out.print("Input the number of day in the year: ");
        int numberOfTheDay = getIntegerFromConsoleInRange(1, 365);
        System.out.println(getStatusOfTheDay(numberOfTheDay));
    }

    static String getStatusOfTheDay(int numberOfTheDay) {
        return numberOfTheDay % 6 == 0 || numberOfTheDay % 7 == 0 ? "Weekend" : "Workday";
    }
}
