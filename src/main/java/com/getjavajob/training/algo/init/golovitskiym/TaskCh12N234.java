package com.getjavajob.training.algo.init.golovitskiym;

import java.util.Random;

import static com.getjavajob.training.algo.util.DataFromConsole.getIntegerFromConsole;

public class TaskCh12N234 {
    public static void main(String[] args) {
        System.out.print("Input the number of rows in array: ");
        int rows = getIntegerFromConsole();
        System.out.print("Input the number of columns in array: ");
        int columns = getIntegerFromConsole();
        int[][] array = new int[rows][columns];
        Random randomNumber = new Random();
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = randomNumber.nextInt(20);
            }
        }
        System.out.println("You got an array: ");
        print2DArray(array);
        System.out.print("Input the row number to delete: ");
        int rowNumber = getIntegerFromConsole();
        System.out.println("New array: ");
        print2DArray(deleteRow(array, rowNumber));
        System.out.print("Input the column number to delete: ");
        int columnNumber = getIntegerFromConsole();
        System.out.println("New array: ");
        print2DArray(deleteColumn(array, columnNumber));
    }

    static private void print2DArray(int[][] array) {
        for (int[] rows : array) {
            for (int element : rows) {
                System.out.print(element + "\t");
            }
            System.out.println();
        }
    }

    static int[][] deleteRow(int[][] array, int rowNumber) {
        int[][] resultArray = new int[array.length][array.length];
        for (int i = 0; i < resultArray.length; i++) {
            for (int j = 0; j < resultArray[i].length; j++) {
                resultArray[i][j] = 0;
            }
        }
        System.arraycopy(array, 0, resultArray, 0, rowNumber - 1);
        System.arraycopy(array, rowNumber, resultArray, rowNumber - 1, array.length - rowNumber);
        return resultArray;
    }

    static int[][] deleteColumn(int[][] array, int columnNumber) {
        int[][] resultArray = new int[array.length][array.length];
        for (int i = 0; i < resultArray.length; i++) {
            for (int j = 0; j < resultArray[i].length; j++) {
                resultArray[i][j] = 0;
            }
        }
        for (int i = 0; i < resultArray.length; i++) {
            System.arraycopy(array[i], 0, resultArray[i], 0, columnNumber - 1);
            System.arraycopy(array[i], columnNumber, resultArray[i], columnNumber - 1, array[i].length - columnNumber);
        }
        return resultArray;
    }
}
