package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh09N107.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N107Test {
    public static void main(String[] args) {
        testChangeLetters();
    }

    private static void testChangeLetters() {
        assertEquals("TaskCh09N107Test.testChangeLetters", "Metollica", changeLetters("Metallico"));
    }
}
