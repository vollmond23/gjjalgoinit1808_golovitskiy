package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh10N052.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N052Test {
    public static void main(String[] args) {
        testGetDigitsBackwards();
    }

    private static void testGetDigitsBackwards() {
        assertEquals("TaskCh10N052Test.testGetDigitsBackwards", 54321, getDigitsBackwards(12345));
    }
}
