package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.*;
import static java.lang.Math.round;

public class TaskCh10N042 {
    public static void main(String[] args) {
        System.out.print("Input a real number: ");
        double realNumber = getDoubleFromConsole();
        System.out.print("Input a power: ");
        int power = getIntegerFromConsole();
        System.out.println("Result: " + raiseNumberToPower(realNumber, power));
    }

    static double raiseNumberToPower(double realNumber, int power) {
        if (power == 1) {
            return realNumber;
        }
        return round(realNumber * raiseNumberToPower(realNumber, power - 1)  * 100.0) / 100.0;
    }
}
