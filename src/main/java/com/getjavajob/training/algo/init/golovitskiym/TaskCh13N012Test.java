package com.getjavajob.training.algo.init.golovitskiym;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh13N012Test {
    public static void main(String[] args) {
        testFindMatches();
        testGetWorkingPeriod();
    }

    private static void testFindMatches() {
        Database db = new Database();
        db.addEmployee("Ivanov", "Petr", "Semyonych", "Moscow", 2001, 1);
        db.addEmployee("Petrov", "Ivan", "Moscow", 2002, 2);
        db.addEmployee("Sidorov", "Mikhail", "Ivanovich", "Moscow", 2003, 3);
        db.addEmployee("Kozlov", "Andrey", "Artemovich", "Moscow", 2015, 4);
        db.addEmployee("Ivanova", "Ksenia", "Moscow", 2017, 5);
        db.addEmployee("Myshkin", "Knyaz", "Vladlenovich", "Moscow", 2001, 5);
        db.addEmployee("Novikov", "Sergey", "Sergeevich", "Moscow", 2015, 6);
        db.addEmployee("Belova", "Anastasia", "Petrovna", "Moscow", 2013, 7);
        db.addEmployee("Kruglov", "Evgeniy", "Moscow", 2015, 7);
        db.addEmployee("Dzyuba", "Artem", "Moscow", 2018, 1);
        db.addEmployee("Kashtanov", "Kashtan", "Kashtanovich", "Moscow", 2005, 4);
        db.addEmployee("Ivanov", "Leonid", "Semyonych", "Moscow", 2005, 2);
        db.addEmployee("Kramarev", "Saveliy", "Moscow", 2013, 12);
        db.addEmployee("Suchkov", "Ivan", "Vladimirovich", "Moscow", 2012, 11);
        db.addEmployee("Lipin", "Lavrentiy", "Pavlovich", "Moscow", 2015, 10);
        db.addEmployee("Kuklin", "Aleksey", "Evgenievich", "Moscow", 1998, 4);
        db.addEmployee("Kuklina", "Olga", "Vladimirovna", "Moscow", 1997, 6);
        db.addEmployee("Ryabov", "Mikhail", "Sergeevich", "Moscow", 2016, 5);
        db.addEmployee("Kirin", "Kirill", "Ashotovich", "Moscow", 2015, 7);
        db.addEmployee("Vasiliev", "Ashot", "Moscow", 2009, 1);

        List<Employee> expectedList = new ArrayList<>();
        expectedList.add(new Employee("Ivanov", "Petr", "Semyonych", "Moscow", new GregorianCalendar(2001, 1, 1).getTime()));
        expectedList.add(new Employee("Petrov", "Ivan", "Moscow", new GregorianCalendar(2002, 2, 1).getTime()));
        expectedList.add(new Employee("Sidorov", "Mikhail", "Ivanovich", "Moscow", new GregorianCalendar(2003, 3, 1).getTime()));
        expectedList.add(new Employee("Ivanova", "Ksenia", "Moscow", new GregorianCalendar(2017, 5, 1).getTime()));
        expectedList.add(new Employee("Ivanov", "Leonid", "Semyonych", "Moscow", new GregorianCalendar(2005, 2, 1).getTime()));
        expectedList.add(new Employee("Suchkov", "Ivan", "Vladimirovich", "Moscow", new GregorianCalendar(2012, 11, 1).getTime()));

        assertEquals("TaskCh13N012Test.testFindMatches", expectedList, db.findMatches("Ivan"));
    }

    private static void testGetWorkingPeriod() {
        Employee employee = new Employee("Ivanov", "Petr", "Semyonych", "Moscow", new GregorianCalendar(2001, 1, 1).getTime());
        assertEquals("TaskCh13N012Test.testGetWorkingPeriod", 17, employee.getWorkingPeriod(3, 2018));
    }
}
