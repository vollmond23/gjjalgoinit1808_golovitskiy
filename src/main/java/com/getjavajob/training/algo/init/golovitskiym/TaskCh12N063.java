package com.getjavajob.training.algo.init.golovitskiym;

import java.util.Arrays;
import java.util.Random;

public class TaskCh12N063 {
    public static void main(String[] args) {
        int[][] studentsCounts = new int[11][4];
        Random randomNumber = new Random();
        for (int i = 0; i < studentsCounts.length; i++) {
            for (int j = 0; j < 4; j++) {
                studentsCounts[i][j] = randomNumber.nextInt(10) + 25;
            }
        }
        System.out.print("Students counts average for parallels: ");
        System.out.println(Arrays.toString(getStudentsCountsAverage(studentsCounts)));
    }

    static double[] getStudentsCountsAverage(int[][] array) {
        double[] resultArray = new double[array.length];
        double count = 0.0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                count += array[i][j];
            }
            resultArray[i] = count / array[i].length;
            count = 0.0;
        }
        return resultArray;
    }
}
