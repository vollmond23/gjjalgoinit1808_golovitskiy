package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.*;

public class TaskCh01N003 {
    public static void main(String[] args) {
        System.out.println("Input an integer number.");
        int number = getIntegerFromConsole();
        System.out.println(outputString(number));
    }

    static private String outputString(int number) {
        return "You have input: " + number;
    }
}
