package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.*;

public class TaskCh04N033 {
    public static void main(String[] args) {
        int number = getIntegerFromConsole();
        System.out.println(checkEven(number) ? "Last digit is even." : "Last digit is odd.");
    }

    static boolean checkEven(int number) {
        return number % 2 == 0;
    }
}
