package com.getjavajob.training.algo.init.golovitskiym;

import java.util.Arrays;

import static com.getjavajob.training.algo.util.DataFromConsole.getIntegerFromConsole;

public class TaskCh12N024 {
    public static void main(String[] args) {
        System.out.print("Input the size of array: ");
        int arraySize = getIntegerFromConsole();
        System.out.println(Arrays.deepToString(fillUpArrayTaskA(arraySize)));
        System.out.println(Arrays.deepToString(fillUpArrayTaskB(arraySize)));
    }

    static int[][] fillUpArrayTaskA(int arraySize) {
        int[][] resultArray = new int[arraySize][arraySize];
        for (int i = 0; i < resultArray.length; i++) {
            for (int j = 0; j < resultArray[i].length; j++) {
                resultArray[i][j] = (i == 0 || j == 0) ? 1 : resultArray[i - 1][j] + resultArray[i][j - 1];
            }
        }
        return resultArray;
    }

    static int[][] fillUpArrayTaskB(int arraySize) {
        int[][] resultArray = new int[arraySize][arraySize];
        for (int i = 0; i < resultArray.length; i++) {
            for (int j = 0; j < resultArray[i].length; j++) {
                resultArray[i][j] = (i + j) % arraySize + 1;
            }
        }
        return resultArray;
    }
}
