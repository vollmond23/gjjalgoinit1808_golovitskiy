package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh10N053.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N053Test {
    public static void main(String[] args) {
        testFlipArrayBackward();
    }

    private static void testFlipArrayBackward() {
        int[] expectedArray = {5, 4, 3, 2, 1};
        int[] actualArray = {1, 2, 3, 4, 5};
        assertEquals("TaskCh10N053Test.testFlipArrayBackward", expectedArray, flipArrayBackward(actualArray, 0));
    }
}
