package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh04N067.*;
import static com.getjavajob.training.algo.util.Assert.*;

public class TaskCh04N067Test {
    public static void main(String[] args) {
        testWorkday();
        testWeekend();
    }

    private static void testWorkday() {
        assertEquals("TaskCh04N067Test.testWorkday", "Workday", getStatusOfTheDay(5));
    }

    private static void testWeekend() {
        assertEquals("TaskCh04N067Test.testWeekend", "Weekend", getStatusOfTheDay(7));
    }
}
