package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh11N245.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh11N245Test {
    public static void main(String[] args) {
        testPutNegativeAtTheBeginning();
    }

    private static void testPutNegativeAtTheBeginning() {
        int[] expectedArray = {-6, -4, -2, 1, 3, 5};
        int[] actualArray = {1, -2, 3, -4, 5, -6};
        assertEquals("TaskCh11N245Test.testPutNegativeAtTheBeginning", expectedArray, putNegativeAtTheBeginning(actualArray));
    }
}
