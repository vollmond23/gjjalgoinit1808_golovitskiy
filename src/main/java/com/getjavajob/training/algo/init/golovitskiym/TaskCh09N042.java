package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.*;

public class TaskCh09N042 {
    public static void main(String[] args) {
        System.out.print("Input a word: ");
        String word = getStringFromConsole();
        System.out.println("Reversed word: " + reverseWord(word));
    }

    static String reverseWord(String word) {
        StringBuilder stringBuilder = new StringBuilder(word);
        return stringBuilder.reverse().toString();
    }
}
