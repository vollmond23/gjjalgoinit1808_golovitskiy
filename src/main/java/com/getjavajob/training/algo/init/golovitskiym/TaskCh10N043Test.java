package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh10N043.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N043Test {
    public static void main(String[] args) {
        testAddDigitsTogether();
        testCountDigits();
    }

    private static void testAddDigitsTogether() {
        assertEquals("TaskCh10N043Test.testAddDigitsTogether", 10, addDigitsTogether(1234));
    }

    private static void testCountDigits() {
        assertEquals("TaskCh10N043Test.testCountDigits", 4, countDigits(1234));
    }
}
