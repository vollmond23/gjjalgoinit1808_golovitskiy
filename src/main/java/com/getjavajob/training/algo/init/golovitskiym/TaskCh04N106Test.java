package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh04N106.*;
import static com.getjavajob.training.algo.util.Assert.*;

public class TaskCh04N106Test {
    public static void main(String[] args) {
        testWinter();
        testSpring();
        testSummer();
        testAutumn();
    }

    private static void testWinter() {
        assertEquals("TaskCh04N106Test.testWinter", "Winter", getSeason(12));
    }

    private static void testSpring() {
        assertEquals("TaskCh04N106Test.testSpring", "Spring", getSeason(3));
    }

    private static void testSummer() {
        assertEquals("TaskCh04N106Test.testSummer", "Summer", getSeason(7));
    }

    private static void testAutumn() {
        assertEquals("TaskCh04N106Test.testAutumn", "Autumn", getSeason(10));
    }
}
