package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.*;

public class TaskCh09N166 {
    public static void main(String[] args) {
        System.out.print("Input a phrase: ");
        String phrase = getStringFromConsole();
        System.out.println(swapFirstAndLastWords(phrase));
    }

    static String swapFirstAndLastWords(String phrase) {
        String[] wordsInPhrase = phrase.split(" ");
        if (wordsInPhrase.length < 2) {
            return "Error. Phrase should contain at least 2 words.";
        } else {
            StringBuilder changedPhrase = new StringBuilder();
            changedPhrase.append(wordsInPhrase[wordsInPhrase.length - 1]);
            for (int i = 1; i < wordsInPhrase.length - 1; i++) {
                changedPhrase.append(' ').append(wordsInPhrase[i]);
            }
            changedPhrase.append(' ').append(wordsInPhrase[0]);
            return changedPhrase.toString();
        }
    }
}
