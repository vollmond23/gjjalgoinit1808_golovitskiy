package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh09N042.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N042Test {
    public static void main(String[] args) {
        testReverseWord();
    }

    private static void testReverseWord() {
        assertEquals("TaskCh09N042Test.testReverseWord", "airA", reverseWord("Aria"));
    }
}
