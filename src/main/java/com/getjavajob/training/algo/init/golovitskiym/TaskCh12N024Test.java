package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh12N024.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N024Test {
    public static void main(String[] args) {
        testFillUpArrayTaskA();
        testFillUpArrayTaskB();
    }

    private static void testFillUpArrayTaskA() {
        int[][] expectedArray = {
                {1, 1, 1, 1, 1, 1},
                {1, 2, 3, 4, 5, 6},
                {1, 3, 6, 10, 15, 21},
                {1, 4, 10, 20, 35, 56},
                {1, 5, 15, 35, 70, 126},
                {1, 6, 21, 56, 126, 252}};
        assertEquals("TaskCh12N024Test.testFillUpArrayTaskA", expectedArray, fillUpArrayTaskA(6));
    }

    private static void testFillUpArrayTaskB() {
        int[][] expectedArray = {
                {1, 2, 3, 4, 5, 6},
                {2, 3, 4, 5, 6, 1},
                {3, 4, 5, 6, 1, 2},
                {4, 5, 6, 1, 2, 3},
                {5, 6, 1, 2, 3, 4},
                {6, 1, 2, 3, 4, 5}};
        assertEquals("TaskCh12N024Test.testFillUpArrayTaskB", expectedArray, fillUpArrayTaskB(6));
    }
}
