package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.golovitskiym.TaskCh02N043.*;

public class TaskCh02N043Test {
    public static void main(String[] args) {
        testDivided();
        testNotDivided();
    }

    private static void testDivided() {
        assertEquals("TaskCh02N043Test.testDivided", 1, returnSolution(2, 4));
    }

    private static void testNotDivided() {
        assertEquals("TaskCh02N043Test.testNotDivided", 0, returnSolution(2, 3));
    }
}
