package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.util.DataFromConsole.getStringFromConsole;

public class TaskCh09N107 {
    public static void main(String[] args) {
        System.out.print("Input a word: ");
        String word = getStringFromConsole();
        System.out.println("Changed word: " + changeLetters(word));
    }

    static String changeLetters(String word) {
        int indexOfFirstA = word.indexOf('a');
        int indexOfLastO = word.lastIndexOf('o');
        if (indexOfFirstA < 0 || indexOfLastO < 0) {
            return "'a' or 'o' not found at \"" + word + "\"";
        } else {
            return word.substring(0, indexOfFirstA) + 'o'
                    + word.substring(indexOfFirstA + 1, indexOfLastO) + 'a'
                    + word.substring(indexOfLastO + 1);
        }
    }
}
