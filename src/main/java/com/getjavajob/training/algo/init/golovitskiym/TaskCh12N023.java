package com.getjavajob.training.algo.init.golovitskiym;

import java.util.Arrays;

import static com.getjavajob.training.algo.util.DataFromConsole.getIntegerFromConsole;

public class TaskCh12N023 {
    public static void main(String[] args) {
        System.out.print("Input the size of array: ");
        int arraySize = getOddIntegerFromConsole();
        System.out.println(Arrays.deepToString(fillUpArrayTaskA(arraySize)));
        System.out.println(Arrays.deepToString(fillUpArrayTaskB(arraySize)));
        System.out.println(Arrays.deepToString(fillUpArrayTaskC(arraySize)));
    }

    static int[][] fillUpArrayTaskA(int arraySize) {
        int[][] resultArray = new int[arraySize][arraySize];
        for (int i = 0; i < resultArray.length; i++) {
            for (int j = 0; j < resultArray[i].length; j++) {
                resultArray[i][j] = (i == j || j == arraySize - i - 1) ? 1 : 0;
            }
        }
        return resultArray;
    }

    static int[][] fillUpArrayTaskB(int arraySize) {
        int[][] resultArray = new int[arraySize][arraySize];
        for (int i = 0; i < resultArray.length; i++) {
            for (int j = 0; j < resultArray[i].length; j++) {
                resultArray[i][j] = (i == j || j == arraySize - i - 1 || i == arraySize / 2 || j == arraySize / 2) ? 1 : 0;
            }
        }
        return resultArray;
    }

    static int[][] fillUpArrayTaskC(int arraySize) {
        int[][] resultArray = new int[arraySize][arraySize];
        for (int i = 0; i < resultArray.length; i++) {
            for (int j = 0; j < resultArray[i].length; j++) {
                resultArray[i][j] = (j >= i && j <= arraySize - i - 1 || j <= i && j >= arraySize - i - 1) ? 1 : 0;
            }
        }
        return resultArray;
    }

    static private int getOddIntegerFromConsole() {
        int number;
        while (true) {
            number = getIntegerFromConsole();
            if (number < 0 && number % 2 == 0) {
                System.out.println("Number should be odd and bigger then 0.");
            } else {
                break;
            }
        }
        return number;
    }
}
