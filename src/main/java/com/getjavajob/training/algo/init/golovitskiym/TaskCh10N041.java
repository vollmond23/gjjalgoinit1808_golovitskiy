package com.getjavajob.training.algo.init.golovitskiym;

import java.math.BigInteger;

import static com.getjavajob.training.algo.util.DataFromConsole.*;

public class TaskCh10N041 {
    public static void main(String[] args) {
        System.out.print("Input a number: ");
        int n = getIntegerFromConsole();
        System.out.println("Factorial of " + n + " is " + factorial(n));
    }

    static BigInteger factorial(int n) {
        if (n == 1) {
            return BigInteger.valueOf(1);
        }
        return factorial(n - 1).multiply(BigInteger.valueOf(n));
    }
}
