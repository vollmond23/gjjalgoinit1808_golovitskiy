package com.getjavajob.training.algo.init.golovitskiym;

import static com.getjavajob.training.algo.init.golovitskiym.TaskCh11N158.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh11N158Test {
    public static void main(String[] args) {
        testDeleteRepeatedElements();
    }

    private static void testDeleteRepeatedElements() {
        int[] expectedArray = {1, 2, 3, 0, 0, 0};
        int[] actualArray = {1, 2, 3, 1, 2, 3};
        assertEquals("TaskCh11N158Test.testDeleteRepeatedElements", expectedArray, deleteRepeatedElements(actualArray));
    }
}
